<?php

namespace Modules\TechlifyVehicleBasic\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\LoggableModel;

class VehicleColor extends Model
{
    use LoggableModel;
    use SoftDeletes;
    protected $fillable = [];

    protected $table = "vehicle_colors";

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'LIKE', '%' . $filters['search'] . '%');
        }

        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        $query->where(function ($q) {
            $q->whereNull('client_id')
                ->orWhere('client_id', auth()->user()->client_id);
        });
    }
}
