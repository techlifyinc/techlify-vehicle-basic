<?php

namespace Modules\TechlifyVehicleBasic\Entities;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\LaravelCore\Entities\LoggableModel;

class VehicleModel extends Model
{
    use LoggableModel;
    use SoftDeletes;

    protected $fillable = [];

    protected $table = "vehicle_models";

    protected $with = [
        'make',
    ];

    protected $appends = ['label_title'];

    public function getLabelTitleAttribute()
    {
        return $this->title . ' - ' . ($this->make->title ?? '');
    }

    public function creator()
    {
        return $this->belongsTo(User::class, 'creator_id', 'id');
    }

    public function make()
    {
        return $this->belongsTo(VehicleMake::class, 'make_id', 'id');
    }

    public function scopeFilter($query, $filters)
    {
        if (isset($filters['sort_by']) && "" != trim($filters['sort_by'])) {
            $sort = explode("|", $filters['sort_by']);
            $query->orderBy($sort[0], $sort[1]);
        }

        if (isset($filters['search']) && "" != trim($filters['search'])) {
            $query->where('title', 'LIKE', '%' . $filters['search'] . '%');
        }

        if (isset($filters['make_id']) && is_numeric($filters['make_id'])) {
            $query->whereHas('make', function ($q) use ($filters) {
                $q->where('id', $filters['make_id']);
            });
        }

        $query->where(function($q) {
            $q->whereNull('client_id')
                ->orWhere('client_id', auth()->user()->client_id);
        });
    }
}
