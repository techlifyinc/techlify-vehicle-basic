<?php

namespace Modules\TechlifyVehicleBasic\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\TechlifyVehicleBasic\Entities\VehicleModel;

class VehicleModelController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $filters = request([
            'sort_by',
            'search',
            'make_id'
        ]);
        $perPage = request()->query('per_page');

        $with = [];
        if ($request->has('with') && $request->with) {
            $with = explode(',',    $request->with);
        }

        if (empty($perPage)) {
            return ["data" => VehicleModel::filter($filters)->with($with)->get()];
        }

        return VehicleModel::filter($filters)->with($with)->paginate($perPage);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $model = new VehicleModel();

        $rules = [
            "title" => "required|string",
            "make_id" => "required|exists:vehicle_makes,id",
        ];

        $request->validate($rules);

        $model->title = request('title');
        $model->description = request('description', '');
        $model->make_id = request('make_id');
        $model->creator_id = auth()->id();
        $model->client_id = auth()->user()->client_id;

        if (!$model->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $model->load('make');
        return response()->json(['item' => $model], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, VehicleModel $model)
    {
        $rules = [
            "title" => "required|string",
            "make_id" => "required|exists:vehicle_makes,id",
        ];

        $request->validate($rules);

        $model->title = request('title');
        $model->description = request('description', '');
        $model->make_id = request('make_id');
        if (!$model->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $model->load('make');

        return response()->json(['item' => $model], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(VehicleModel $model)
    {
        if ($model->delete()) {
            return response()->json(["item" => $model], Response::HTTP_OK);
        }
        return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
    }
}
