<?php

namespace Modules\TechlifyVehicleBasic\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\TechlifyVehicleBasic\Entities\VehicleColor;

class VehicleColorController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $filters = request([
            'sort_by',
            'search',
        ]);

        $perPage = request()->query('per_page');

        $with = [];
        if ($request->has('with') && $request->with) {
            $with = explode(',',    $request->with);
        }

        if (empty($perPage)) {
            return ["data" => VehicleColor::filter($filters)->with($with)->get()];
        }

        return VehicleColor::filter($filters)->with($with)->paginate($perPage);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $rules = [
            "title" => "required|string",
        ];

        $request->validate($rules);

        $color = new VehicleColor();

        $color->title = request('title');
        $color->creator_id = auth()->id();
        $color->client_id = auth()->user()->client_id;

        if (!$color->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return response()->json(['item' => $color], Response::HTTP_CREATED);
    }

    public function update(Request $request, VehicleColor $color)
    {
        $rules = [
            "title" => "required|string",
        ];

        $request->validate($rules);


        $color->title = request('title');
        if (!$color->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        return response()->json(['item' => $color], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(VehicleColor $color)
    {
        if ($color->delete()) {
            return response()->json(["item" => $color], Response::HTTP_OK);
        }
        return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
    }
}
