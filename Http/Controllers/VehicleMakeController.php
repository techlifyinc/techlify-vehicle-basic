<?php

namespace Modules\TechlifyVehicleBasic\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\TechlifyVehicleBasic\Entities\VehicleMake;

class VehicleMakeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $filters = request([
            'sort_by',
            'search',
        ]);
        $perPage = request()->query('per_page');

        $with = [];
        if ($request->has('with') && $request->with) {
            $with = explode(',',    $request->with);
        }
        if (empty($perPage)) {
            return ["data" => VehicleMake::filter($filters)->with($with)->get()];
        }

        return VehicleMake::filter($filters)->with($with)->paginate($perPage);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $make = new VehicleMake();

        $rules = [
            "title" => "required|string",
        ];

        $request->validate($rules);

        $make->title = request('title');
        $make->description = request('description', '');
        $make->creator_id = auth()->id();
        $make->client_id = auth()->user()->client_id;
        if (!$make->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return response()->json(['item' => $make], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, VehicleMake $make)
    {
        $rules = [
            "title" => "required|string",
        ];

        $request->validate($rules);

        $make->title = request('title');
        $make->description = request('description', '');
        if (!$make->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return response()->json(['item' => $make], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(VehicleMake $make)
    {
        if ($make->delete()) {
            return response()->json(["item" => $make], Response::HTTP_OK);
        }
        return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
    }
}
