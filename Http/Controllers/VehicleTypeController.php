<?php

namespace Modules\TechlifyVehicleBasic\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\TechlifyVehicleBasic\Entities\VehicleType;

class VehicleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        $filters = request([
            'sort_by',
            'search',
        ]);
        $perPage = request()->query('per_page');

        $with = [];
        if ($request->has('with') && $request->with) {
            $with = explode(',',    $request->with);
        }
        if (empty($perPage)) {
            return ["data" => VehicleType::filter($filters)->with($with)->get()];
        }

        return VehicleType::filter($filters)->with($with)->paginate($perPage);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $type = new VehicleType();

        $rules = [
            "title" => "required|string",
        ];

        $request->validate($rules);

        $type->title = request('title');
        $type->description = request('description', '');
        $type->creator_id = auth()->id();
        $type->client_id = auth()->user()->client_id;

        if (!$type->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return response()->json(['item' => $type], Response::HTTP_CREATED);
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, VehicleType $type)
    {
        $rules = [
            "title" => "required|string",
        ];

        $request->validate($rules);

        $type->title = request('title');
        $type->description = request('description', '');
        if (!$type->save()) {
            return response()->json(['error' => "Request cannot be processed."], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return response()->json(['item' => $type], Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy(VehicleType $type)
    {
        if ($type->delete()) {
            return response()->json(["item" => $type], Response::HTTP_OK);
        }
        return response()->json(['errors' => 'Request cannot be processed'], Response::HTTP_BAD_REQUEST);
    }
}
