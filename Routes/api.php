<?php

use Illuminate\Support\Facades\Route;
use Modules\TechlifyVehicleBasic\Http\Controllers\VehicleMakeController;
use Modules\TechlifyVehicleBasic\Http\Controllers\VehicleColorController;
use Modules\TechlifyVehicleBasic\Http\Controllers\VehicleModelController;
use Modules\TechlifyVehicleBasic\Http\Controllers\VehicleTypeController;

//Vehicle Makes
Route::get('vehicle-makes', [VehicleMakeController::class, 'index'])
    ->middleware("TechlifyAccessControl:vehicle_make_read");
Route::post('vehicle-makes', [VehicleMakeController::class, 'store'])
    ->middleware("TechlifyAccessControl:vehicle_make_create");
Route::put('vehicle-makes/{make}', [VehicleMakeController::class, 'update'])
    ->middleware("TechlifyAccessControl:vehicle_make_update");
Route::delete('vehicle-makes/{make}', [VehicleMakeController::class, 'destroy'])
    ->middleware("TechlifyAccessControl:vehicle_make_delete");

//Vehicle Models
Route::get('vehicle-models', [VehicleModelController::class, 'index'])
    ->middleware("TechlifyAccessControl:vehicle_model_read");
Route::post('vehicle-models', [VehicleModelController::class, 'store'])
    ->middleware("TechlifyAccessControl:vehicle_model_create");
Route::put('vehicle-models/{model}', [VehicleModelController::class, 'update'])
    ->middleware("TechlifyAccessControl:vehicle_model_update");
Route::delete('vehicle-models/{model}', [VehicleModelController::class, 'destroy'])
    ->middleware("TechlifyAccessControl:vehicle_model_delete");

//Vehicle Models
Route::get('vehicle-colors', [VehicleColorController::class, 'index'])
    ->middleware("TechlifyAccessControl:vehicle_color_read");
Route::post('vehicle-colors', [VehicleColorController::class, 'store'])
    ->middleware("TechlifyAccessControl:vehicle_color_create");
Route::put('vehicle-colors/{color}', [VehicleColorController::class, 'update'])
    ->middleware("TechlifyAccessControl:vehicle_color_update");
Route::delete('vehicle-colors/{color}', [VehicleColorController::class, 'destroy'])
    ->middleware("TechlifyAccessControl:vehicle_color_delete");

//Vehicle Types
Route::get('vehicle-types', [VehicleTypeController::class, 'index'])
    ->middleware("TechlifyAccessControl:vehicle_type_read");
Route::post('vehicle-types', [VehicleTypeController::class, 'store'])
    ->middleware("TechlifyAccessControl:vehicle_type_create");
Route::put('vehicle-types/{type}', [VehicleTypeController::class, 'update'])
    ->middleware("TechlifyAccessControl:vehicle_type_update");
Route::delete('vehicle-types/{type}', [VehicleTypeController::class, 'destroy'])
    ->middleware("TechlifyAccessControl:vehicle_type_delete");
