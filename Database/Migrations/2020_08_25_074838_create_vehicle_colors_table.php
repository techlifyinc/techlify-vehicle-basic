<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVehicleColorsTable extends Migration
{
    private $permissionColors = [
        ['slug' => 'vehicle_color_create', 'label' => "Vehicle Color : Create"],
        ['slug' => 'vehicle_color_update', 'label' => "Vehicle Color : Update"],
        ['slug' => 'vehicle_color_read', 'label' => "Vehicle Color : Read"],
        ['slug' => 'vehicle_color_delete', 'label' => "Vehicle Color : Delete"],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_colors')) {
            Schema::create('vehicle_colors', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('title');
                $table->unsignedBigInteger('creator_id');
                $table->unsignedBigInteger('client_id')
                    ->nullable()
                    ->default(null);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        $permissions = DB::table('permissions')->whereIn('slug', collect($this->permissionColors)->pluck('slug'))
            ->get();

        if (!count($permissions)) {
            DB::table('permissions')
                ->insert($this->permissionColors);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_colors');

        $slugs = collect($this->permissionColors)->pluck('slug');

        DB::table('permissions')
            ->whereIn("slug", $slugs)
            ->delete();
    }
}
