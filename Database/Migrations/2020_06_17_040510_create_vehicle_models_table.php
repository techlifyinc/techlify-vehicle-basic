<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVehicleModelsTable extends Migration
{
    private $permissionModels = [
        ['slug' => 'vehicle_model_create', 'label' => "Vehicle Model : Create"],
        ['slug' => 'vehicle_model_update', 'label' => "Vehicle Model : Update"],
        ['slug' => 'vehicle_model_read', 'label' => "Vehicle Model : Read"],
        ['slug' => 'vehicle_model_delete', 'label' => "Vehicle Model : Delete"],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_models')) {
            Schema::create('vehicle_models', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedBigInteger('make_id');
                $table->string('title');
                $table->text('description');
                $table->unsignedBigInteger('creator_id');
                $table->unsignedBigInteger('client_id')
                    ->nullable()
                    ->default(null);
                $table->timestamps();
                $table->softDeletes();

                //Foriegn key constraints
                $table->foreign('make_id')
                    ->references('id')->on('vehicle_makes')
                    ->onDelete('cascade');
            });
        }

        $permissions = DB::table('permissions')->whereIn('slug', collect($this->permissionModels)->pluck('slug'))
            ->get();

        if (!count($permissions)) {
            DB::table('permissions')
                ->insert($this->permissionModels);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_models');

        $slugs = collect($this->permissionModels)->pluck('slug');

        DB::table('permissions')
            ->whereIn("slug", $slugs)
            ->delete();
    }
}
