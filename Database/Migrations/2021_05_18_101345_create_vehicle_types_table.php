<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateVehicleTypesTable extends Migration
{
    private $permissionTypes = [
        ['slug' => 'vehicle_type_create', 'label' => "Vehicle Type : Create"],
        ['slug' => 'vehicle_type_update', 'label' => "Vehicle Type : Update"],
        ['slug' => 'vehicle_type_read', 'label' => "Vehicle Type : Read"],
        ['slug' => 'vehicle_type_delete', 'label' => "Vehicle Type : Delete"],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_types')) {
            Schema::create('vehicle_types', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('title');
                $table->text('description');
                $table->unsignedBigInteger('creator_id');
                $table->unsignedBigInteger('client_id')
                    ->nullable()
                    ->default(null);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        $permissions = DB::table('permissions')->whereIn('slug', collect($this->permissionTypes)->pluck('slug'))
            ->get();

        if (!count($permissions)) {
            DB::table('permissions')
                ->insert($this->permissionTypes);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_types');

        $slugs = collect($this->permissionTypes)->pluck('slug');

        DB::table('permissions')
            ->whereIn("slug", $slugs)
            ->delete();
    }
}
