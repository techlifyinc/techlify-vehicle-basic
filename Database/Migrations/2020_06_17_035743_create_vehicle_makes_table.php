<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateVehicleMakesTable extends Migration
{
    private $permissionModels = [
        ['slug' => 'vehicle_make_create', 'label' => "Vehicle Make : Create"],
        ['slug' => 'vehicle_make_update', 'label' => "Vehicle Make : Update"],
        ['slug' => 'vehicle_make_read', 'label' => "Vehicle Make : Read"],
        ['slug' => 'vehicle_make_delete', 'label' => "Vehicle Make : Delete"],
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('vehicle_makes')) {
            Schema::create('vehicle_makes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('title');
                $table->text('description');
                $table->unsignedBigInteger('creator_id');
                $table->unsignedBigInteger('client_id')
                    ->nullable()
                    ->default(null);
                $table->timestamps();
                $table->softDeletes();
            });
        }

        $permissions = DB::table('permissions')->whereIn('slug', collect($this->permissionModels)->pluck('slug'))
            ->get();

        if (!count($permissions)) {
            DB::table('permissions')
                ->insert($this->permissionModels);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_makes');

        $slugs = collect($this->permissionModels)->pluck('slug');

        DB::table('permissions')
            ->whereIn("slug", $slugs)
            ->delete();
    }
}
